/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This module provides common utilities for covidbot
 */


// core dependencies
const cheerio = require('cheerio');
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
const fs = require('fs');
const path = require('path');


// Mastobot setup
const { MastoBot, APIError } = require('mastobot');

// === bot configuration ===
const api_url = require('process').env.API_URL;
const api_token = require('process').env.ACCESS_TOKEN;
const client_id = require('process').env.CLIENT_ID;
const client_secret = require('process').env.CLIENT_SECRET;
// ===== 8< =====

const bot = new MastoBot(api_url, { api_token, client_id, client_secret });


/**
 *  This callback processes CSV data into text
 *  
 *  @callback TextCallback
 *  @param {*}        today     The latest (usually "today") row from the csv data
 *  @param {Array<*>} rows      All filtered rows
 *  @return {string|string[]}   Callback should return a string or array thereof
 */
/**
 *  This callback processes CSV data into a boolean to filter csv data
 *  
 *  @callback FilterCallback
 *  @param {*}        row       The latest (usually "today") row from the csv data
 *  @return {boolean}           Callback should return a truthy value to iinclude the row
 */
/**
 *  @typedef {object} PostResult
 *  @property {boolean}       success       True if the toot was posted
 *  @property {Status}        toot          If posted, the complete Status object
 *  @property {*}             lastRecord    The last record processed (usually "today")
 *  @property {Array<*>}      rows          All rows which passed the filter
 *  @property {Error|*}       error         Downstream error, if any
 */

/**
 *  Fetch a CSV, filter the rows, and post a toot.
 *
 *  @param  {object}          cli           Cli parameters
 *  @param  {number?}         cli.verbose   Print extra debug info
 *  @param  {boolean=}   cli.post      Only post the Toot if true
 *  @param  {string}          url           Data feed to fetch
 *  @param  {string|TextCallback|string[]} spoiler_text  Content warning, 
 *                                array thereof, or TextCallback to generate such
 *  @param  {FilterCallback}  filterFn      Function to filter csv rows
 *  @param  {TextCallback}    tootMaker     Function to generate toot(s)
 *  @return {PostResult}
 */
async function do_csv_post(cli, url, spoiler_text, filterFn, tootMaker) {
  const rows = await fetchCsv(url, filterFn);
  const [ today ] = rows.slice(-1) || [];


  // if we found no relevant data, exit and do nothing
  if (!today)
    return {
      success: true,
      lastRecord: today,
      filteredRows: rows,
    };


  if (cli.verbose >= 1)
    console.log('Latest Record', today);

  const toot = tootMaker(today, rows);


  // if we made no toot, exit and do nothing
  if (!toot || !toot.length)
    return {
      success: true,
      lastRecord: today,
      filteredRows: rows,
    };

  if (typeof spoiler_text === 'function')
    spoiler_text = spoiler_text(today, rows);

  logToot(toot, spoiler_text);

  if (!cli.post)
    return {
      success: true,
      lastRecord: today,
      filteredRows: rows,
    };

  return postToot(toot, cli.visibility, spoiler_text)
  .then(postResult => ({
    ...postResult,
    lastRecord: today,
    filteredRows: rows,
  }));
}


/**
 *  Fetch a csv from the web, optionally filter it, and return the rows
 *
 *  @param  {string} url      URL of the feed
 *  @param  {FilterCallback?} filterFn Optional filter function; return true to include a row
 *  @return {Array<*>}        Row object shape/properties depend on the csv header
 */
async function fetchCsv(url, filterFn) {
  console.log(`fetching data from ${url} ...`);

  const response = await fetch(url);

  if (response.status < 200 || response.status > 299) {
    console.error('failed to fetch;', response.status, response.statusText);
    return {
      success: false,
      error: {
        code: response.status,
        message: response.statusText,
      }
    };
  }
  
  return new Promise((resolve, reject) => {
    const parser = require('csv-parse').parse({
      bom: true,
      cast: true,
      columns: true,
      record_delimiter: ['\n', '\r', '\r\n'],
      skip_empty_lines: true,
    });

    const rows = [];
    parser.on('readable', () => {
      let row = false;
      while ((row = parser.read()) !== null) {
        if (!filterFn || filterFn(row))
          rows.push(row);
      }
    });
    parser.on('end', () => {
      resolve(rows);
    });
    parser.on('error', error => {
      reject(error);
    });

    response.body.pipe(parser);
  });
}


/**
 *  Post a toot
 *
 *  @param  {string}        toot          Text of the toot (nyi: array)
 *  @param  {string=}       visibility    Post visibility
 *  @param  {string?}       spoiler_text  CW for the posted toot; default none
 *  @return {PostResult}    The returned postResult will carry the success, and one of
 *                              a `toot` or `error` properties
 */
function postToot(toot, visibility = 'public', spoiler_text = undefined) {
  return bot.checkCredentials()
  .then(() => bot.postStatus(toot, {
    visibility,
    spoiler_text,
  }))
  .then(postResult => {
    const toot = postResult.data;

    if (toot && toot.id)
      console.log(`posted ${toot.visibility} status:`, toot.url);
    return {
      success: postResult.success,
      toot,
    };
  })
  .catch(error => {
    if (error instanceof APIError)
      console.error('Mastodon Error:', error.statusCode, error.message);
    return {
      success: false,
      error,
    }
  });  
}

/** 
 *  This function accepts a number value and returns the value to be placed
 *    in the deltas object
 *
 *  @callback DeltaFilterFn
 *  @param  {number} n    The number to process
 *  @returns {*}          Return the value you want to see in the world
 */
/**
 *  Calculate deltas between two rows for specified columns
 *
 *  @param  {object} today        "today" row
 *  @param  {object} yesterday    "yesterday" row
 *  @param  {object<string:string>} columns   An object mapping output
 *    properties (keys) to column names (values)
 *  @param  {DeltaFilterFn=} filterFn  Optional function to process each delta
 *
 *  @return {object<string:number>}           Returned object will have a
 *    property for each column in `columns`, the value will be the delta
 *    between rows `a` and `b`, as returned by filterFn if provided
 */
function getDeltas(today, yesterday, columns, filterFn) {
  const process = require('process');
  const ob = {};

  for (const col of Object.keys(columns)) {
    if (process.env.DEBUG && process.env.DEBUG.includes('getDeltas'))
      console.log(`227: col=${col}; column=${columns[col]}; today=${today[columns[col]]}; yesterday=${yesterday[columns[col]]}`)
    try {
      ob[col] = parseInt(today[columns[col]], 10) - parseInt(yesterday[columns[col]], 10);
      if (typeof filterFn === 'function')
        ob[col] = filterFn(ob[col]);
    }
    catch (error) {
      ob[col] = undefined;
      ob[`${col}_error`] = error;
    }
  }

  return ob;
}


function logToot(toot, spoiler_text = '') {
  console.log("\n===== Toot to be posted =====");
  if (Array.isArray(toot)) {
    toot.forEach((e, i) => {
      let st = '';
      if (Array.isArray(spoiler_text))  // if spoilers are an array
        if (i < spoiler_text.length)    // ... with an element for the current Toot
          st = spoiler_text[i];         //     then use the matching spoiler
        else
          st = spoiler_text[1];         //     else use the second in the array
      else
        st = spoiler_text;              // else it's a string, use it directly

      logOneToot(e, st);
      console.log(('-'.repeat(20) + ` (${tootLength(e)} c) ---`).slice(-29));
    });
  }
  else {
    logOneToot(toot, spoiler_text);
    console.log(('='.repeat(20) + ` (${tootLength(toot)} c) ===`).slice(-29));
  }

  function logOneToot(toot, spoiler_text = '') {
    spoiler_text && console.log(spoiler_text, '\n' + '-'.repeat(29));
    console.log(toot);
  }
}


/**
 *  Calculate the length of a toot
 *  - https URLs are 23 characters
 *  - "@user@domain" is "@user" long
 *
 *  @param  {string} toot         Text of the Toot
 *  @param  {String} spoiler_text Spoiler text, if any
 *  @return {number}              Character length of the Toot
 */
function tootLength(toot, spoiler_text = '') {
  if (typeof toot !== 'string')
    throw new TypeError('toot parameter must be a single string');
  if (typeof spoiler_text !== 'string')
    throw new TypeError('spoiler_text parameter must be a single string');
  return (spoiler_text + toot)
    .replace(/https:\S+/, 'x'.repeat(23))
    .replace(/(@\S+)@\S+/, '$1')
    .length;
}


function loadLatest(filenameBase) {
  const filePath = path.resolve('data', filenameBase + '.json');
  if (!fs.existsSync(filePath))
    return false;
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}


function saveLatest(filenameBase, date, data) {
  const dir = path.resolve('data');
  if (!fs.existsSync(dir))
    fs.mkdirSync(dir);
  const filePath = path.resolve('data', filenameBase + '.json');
  const wrote = fs.writeFileSync(filePath, JSON.stringify({ date, data }), 'utf8');
  return wrote;
}


Object.assign(module.exports, {
  // Export a shared MastoBot instance
  bot,

  // And some commonly-used modules
  cheerio,
  fetch,

  // And finally, some utility functions
  do_csv_post,
  fetchCsv,
  postToot,
  getDeltas,
  logToot,
  tootLength,
  loadLatest,
  saveLatest,
});
