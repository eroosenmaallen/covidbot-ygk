/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This command fetches hospitalization data from Ontario Public Health and
 *  Toots it.
 *
 *  This is about the simplest possible implementation of a covidbot pipeline.
 */

const { do_csv_post, getDeltas, loadLatest, saveLatest } = require('../lib/util.js')

Object.assign(exports, {
  command: 'hosp [phRegion]',
  description: 'Hospitalization data for an Ontario Public Health region',
  builder: {
    phRegion: {
      desc: 'Public Health region',
      default: require('process').env.OH_REGION,
      type: 'string',
    },
  },
  handler: cli => do_csv_post(
    cli,
    'https://data.ontario.ca/dataset/8f3a449b-bde5-4631-ada6-8bd94dbc7d15/resource/e760480e-1f95-4634-a923-98161cfb02fa/download/region_hospital_icu_covid_data.csv',

    // spoiler_text, via a function:
    today => `Weekly COVID hospitalizations for Ontario ${today.oh_region} Region, ${today.date}`,

    // filterFn(row) => bool:
    row => (row.oh_region === cli.phRegion),

    // tootMaker(today, rows) => string:
    (today) => {
      const latest = loadLatest('hosp');

      if (latest.date === today.date && !cli.force) {
        console.error('Already logged data for ' + latest.date);
        return false;
      }

      const deltas = getDeltas(today, latest.data, {
        hosp: 'hospitalizations',
        crci: 'icu_crci_total',
        current: 'icu_current_covid',
        recovered: 'icu_former_covid',
      }, diff => (diff === 0 ? '(no change)'
          : diff > 0 ? `(+${diff})`
          : `(${diff})`)
      );

      // only save today's number if we're posting the update
      if (cli.post)
        saveLatest('hosp', today.date, today);

      return `Ontario ${today.oh_region} Region hospitalizations, ${today.date}:

 * current COVID-positive patents: ${today.hospitalizations} ${deltas.hosp && deltas.hosp.replace(/(\)?)$/, ` since ${latest.date}$1`)}
 * total patients in Intensive Care Units (ICUs): ${today.icu_crci_total} ${deltas.crci}

 * current ICU patients COVID-positive: ${today.icu_current_covid} ${deltas.current}
 * current ICU patients recovered: ${today.icu_former_covid} ${deltas.recovered}

   #covid #covid19 #Ontario #OntarioCovid`}
  ),
});
